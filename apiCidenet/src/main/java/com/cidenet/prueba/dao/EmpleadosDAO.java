package com.cidenet.prueba.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import com.cidenet.prueba.entities.Empleado;

public interface EmpleadosDAO extends JpaRepository<Empleado, Long>{

}