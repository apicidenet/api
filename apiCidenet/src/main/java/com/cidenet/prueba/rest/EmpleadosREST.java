package com.cidenet.prueba.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cidenet.prueba.dao.EmpleadosDAO;
import com.cidenet.prueba.entities.Empleado;

@RestController
@RequestMapping("empleados")
public class EmpleadosREST {

	@Autowired
	private EmpleadosDAO empleadoDAO;

	// CREACIÓN EMPLEADOS

	@PostMapping
	public ResponseEntity<Empleado> createEmpleado(@RequestBody Empleado empleado) {
		Empleado newEmpleado = empleadoDAO.save(empleado);
		return ResponseEntity.ok(newEmpleado);
	}

	// CONSULTA EMPLEADOS

	@GetMapping("/listar")
	public ResponseEntity<List<Empleado>> getEmpleado() {
		List<Empleado> empleados = empleadoDAO.findAll();
		return ResponseEntity.ok(empleados);
	}

	// CONSULTA EMPLEADO POR ID

	@RequestMapping(value = "{id}")
	public ResponseEntity<Empleado> getEmpleadoById(@PathVariable("id") Long id) {
		Optional<Empleado> optionalEmpleado = empleadoDAO.findById(id);

		if (optionalEmpleado.isPresent()) {
			return ResponseEntity.ok(optionalEmpleado.get());
		} else {
			return ResponseEntity.noContent().build();
		}
	}

	// ACTUALIZACIÓN EMPLEADOS

	@PutMapping()
	public ResponseEntity<Empleado> updateEmpleado(@RequestBody Empleado empleado) {
		Optional<Empleado> optionalEmpleado = empleadoDAO.findById(empleado.getId());

		if (optionalEmpleado.isPresent()) {
			Empleado updateEmpleado = optionalEmpleado.get();

			updateEmpleado.setPrimerNombre(empleado.getPrimerNombre());
			updateEmpleado.setOtroNombre(empleado.getOtroNombre());
			updateEmpleado.setPrimerApellido(empleado.getPrimerApellido());
			updateEmpleado.setSegundoApellido(empleado.getSegundoApellido());
			updateEmpleado.setPais(empleado.getPais());
			updateEmpleado.setTipoId(empleado.getTipoId());
			updateEmpleado.setId(empleado.getId());
			updateEmpleado.setEmail(empleado.getEmail());
			updateEmpleado.setArea(empleado.getArea());

			empleadoDAO.save(updateEmpleado);
			return ResponseEntity.ok(updateEmpleado);
		} else {
			return ResponseEntity.notFound().build();
		}
	}

	// ELIMINACIÓN EMPLEADO POR ID

	@DeleteMapping(value = "{id}")
	public ResponseEntity<Void> deleteEmpleado(@PathVariable("id") Long id) {
		empleadoDAO.deleteById(id);
		return ResponseEntity.ok(null);
	}

}
