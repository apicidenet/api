package com.cidenet.prueba.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Id;

@Entity
@Table(name = "empleados")
public class Empleado {

	// Variables
	// -----------------------------------------------------------------------------------

	@Column(name = "primerApellido", nullable = false, length = 20)
	private String primerApellido;

	@Column(name = "segundoApellido", nullable = false, length = 20)
	private String segundoApellido;

	@Column(name = "primerNombre", nullable = false, length = 20)
	private String primerNombre;

	@Column(name = "otroNombre", nullable = false, length = 50)
	private String otroNombre;

	@Column(name = "pais", nullable = false)
	private String pais;

	@Column(name = "tipoId", nullable = false)
	private String tipoId;

	@Id
	@Column(name = "id", nullable = false, length = 20)
	private Long id;

	@Column(name = "email", nullable = false, length = 300)
	private String email;

	@DateTimeFormat(pattern = "dd/MM/yyyy")
	@Column(name = "fechaIngreso", nullable = false)
	private Date fechaIngreso;

	@Column(name = "area", nullable = false)
	private String area;

	@Column(name = "estado", nullable = false)
	private String estado;

	@Column(name = "fechaRegistro", nullable = false)
	private String fechaRegistro;

	// Getters and Setters
	// -----------------------------------------------------------------------------------

	public String getPrimerApellido() {
		return primerApellido;
	}

	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}

	public String getSegundoApellido() {
		return segundoApellido;
	}

	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}

	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}

	public String getOtroNombre() {
		return otroNombre;
	}

	public void setOtroNombre(String otroNombre) {
		this.otroNombre = otroNombre;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getTipoId() {
		return tipoId;
	}

	public void setTipoId(String tipoId) {
		this.tipoId = tipoId;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Date getFechaIngreso() {
		return fechaIngreso;
	}

	public void setFechaIngreso(Date fechaIngreso) {
		this.fechaIngreso = fechaIngreso;
	}

	public String getArea() {
		return area;
	}

	public void setArea(String area) {
		this.area = area;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaRegistro() {
		return fechaRegistro;
	}

	public void setFechaRegistro(String fechaRegistro) {
		this.fechaRegistro = fechaRegistro;
	}
}
